const phoneNumbersMessages = {
  required: 'Il numero di telefono è obbligatorio',
  numeric: 'Numero di telefono non valido. Utilizza solo caratteri numerici'
}

export default {
  it: {
    attributes: {
      phoneNumber: 'Numero di telefono'
    },
    custom: {
      phoneNumber: phoneNumbersMessages,
      inviteNumber: phoneNumbersMessages,
      authCode: {
        required: 'Il codice è obbligatorio'
      }
    }
  }
}
