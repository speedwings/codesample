export default {
  invite: {
    // OK:
    // TOO_MANY_REQUEST:
    // NOT_AUTHENTICATED:
    INVITED_TIM: 'Il tuo amico è già Cliente TIM. Invita un altro amico',
    // MOO_REQUEST_ERROR:
    // SMS_ERROR:
    SENDER_TO_RECEIVER_LIMIT_REACHED: 'Hai già invitato il tuo amico. Invitane un altro'
  },

  get (group, response) {
    if (!this[group]) return null
    return this[group][response.result] || response.message
  }
}
