export const API = {
  API_URL: 'http://localhost',
  INVITE_PATH: '/invite',
  LIST_PATH: '/invite-list',
  LOGIN_PATH: '/login',
  REQUIRE_PATH: '/require-password',
  VERIFY_PATH: '/verify-password'
}
export const URL = 'http://localhost'

export const GA = {
  ID: 'UA-XXXXXXXXX-X',
  DEBUG: {
    enabled: false
    // trace: true,
    // sendHitTask: false
  }
}
