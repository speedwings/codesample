import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/Home'
import Manage from '@/components/pages/Manage'
import Auth from '@/components/pages/manage/Auth'
import ManageDashboard from '@/components/pages/manage/Dashboard'

import store from '../store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/manage',
      component: Manage,
      children: [
        {
          path: 'auth',
          name: 'Auth',
          component: Auth,
          beforeEnter (to, from, next) {
            next(!store.getters['auth/isAuthenticated'] ? true : '/manage')
          }
        },
        {
          path: '',
          name: 'Dashboard',
          component: ManageDashboard,
          props: true
        }
      ]
    },
    {
      path: '/:section?',
      name: 'Home',
      component: Home,
      props: true
    }
  ]
})
