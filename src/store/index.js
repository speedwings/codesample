import Vue from 'vue'
import Vuex from 'vuex'

import AuthModule from './modules/auth'
import InvitesModule from './modules/invites'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    invites: InvitesModule,
    auth: AuthModule
  },
  strict: debug
})
