import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { API } from '@/config'

const localStorage = window.localStorage
const TOKEN_KEY = 'pa-tim-token'

function tokenFromResponse (response) {
  return response.headers.authorization.replace(/Bearer\s/g, '')
}

function tokenIsExpired (token = null) {
  if (!token) return true
  const jwt = jwtDecode(token)
  const currentTime = new Date().getTime() / 1000
  return (currentTime > jwt.exp)
}

const state = {
  token: null,
  privacy: false
}

const getters = {
  phoneNumber: (state) => {
    return (state.token) ? jwtDecode(state.token).sub : ''
  },
  validToken: (state) => {
    return (!tokenIsExpired(state.token)) ? state.token : null
  },
  uID: (state, getters) => {
    return (getters.validToken) ? jwtDecode(getters.validToken).csub : null
  },
  isAuthenticated: (state) => {
    return state.token !== null
  }
}

const mutations = {
  storeToken (state, token) {
    state.token = token
    localStorage.setItem(TOKEN_KEY, token)
  },
  setPrivacy (state, bool) {
    state.privacy = bool
  },
  deleteToken (state) {
    state.token = null
    localStorage.removeItem(TOKEN_KEY)
  }
}

const actions = {
  init ({ getters, commit, dispatch }) {
    return new Promise((resolve, reject) => {
      if (localStorage.getItem(TOKEN_KEY)) commit('storeToken', localStorage.getItem(TOKEN_KEY))
      dispatch('login')
        .then(r => { resolve(r) })
        .catch(e => { reject(e) })
    })
  },
  login ({ state, commit }) {
    return new Promise((resolve, reject) => {
      axios.get(API.API_URL + API.LOGIN_PATH, {
        headers: {
          Authorization: (state.token) ? 'Bearer ' + state.token : ''
        }
      }).then((response) => {
        const token = tokenFromResponse(response)
        commit('storeToken', token)
        commit('setPrivacy', response.data.privacy)
        resolve(response)
      }).catch((error) => {
        commit('deleteToken')
        reject(error)
      })
    })
  },
  requirePassword ({ state, commit }, number) {
    return new Promise((resolve, reject) => {
      axios.post(API.API_URL + API.REQUIRE_PATH, {
        msisdn: number,
        flagOtp: true
      }).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  verifyPassword ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      axios.post(API.API_URL + API.VERIFY_PATH, payload)
               .then((response) => {
                 const token = tokenFromResponse(response)
                 commit('storeToken', token)
                 commit('setPrivacy', response.data.privacy)
                 resolve(response.data.token)
               })
               .catch((error) => {
                 reject(error)
               })
    })
  },
  logout ({ commit }) {
    return new Promise((resolve, reject) => {
      commit('deleteToken')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
