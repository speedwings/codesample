import axios from 'axios'
import { API } from '@/config'

const gbPerFriend = 2

const state = {
  pendingInvite: '',
  list: [],
  bonuses: 0
}

const getters = {
  earnedGb: (state) => {
    return state.bonuses * gbPerFriend
  },
  token: (state, getters, rootState, rootGetters) => {
    return rootGetters['auth/validToken']
  }
}

const mutations = {
  setPendingInvite (state, number) {
    state.pendingInvite = number
  },
  setList (state, list) {
    state.list = list
  },
  setBonuses (state, bonuses) {
    state.bonuses = bonuses
  }
}

const actions = {
  getInviteList ({ getters, commit }) {
    return new Promise((resolve, reject) => {
      axios.get(API.API_URL + API.LIST_PATH, {
        headers: {
          Authorization: (getters.token) ? 'Bearer ' + getters.token : ''
        }
      }).then(response => {
        commit('setList', response.data.invited)
        commit('setBonuses', response.data.bonusCounter)
        resolve(response)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  invite ({ getters, commit, dispatch }, payload) {
    return new Promise((resolve, reject) => {
      axios.post(API.API_URL + API.INVITE_PATH, payload, {
        headers: {
          Authorization: (getters.token) ? 'Bearer ' + getters.token : ''
        }
      }).then(response => {
        commit('setPendingInvite', '')
        dispatch('getInviteList')
        .then(() => { resolve(response) })
        .catch(e => { reject(e) })
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
