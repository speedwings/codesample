// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
window.$ = window.jQuery = require('jquery')
import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'

UIkit.use(Icons)

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

// Google Analytics
import VueAnalytics from 'vue-analytics'
import { GA } from '@/config'

Vue.use(VueAnalytics, {
  id: GA.ID,
  router,
  debug: GA.DEBUG
})

// VeeValidate
import it from 'vee-validate/dist/locale/it'
import dict from './validator/dictionary'
import VeeValidate, { Validator } from 'vee-validate'

Validator.addLocale(it)
Validator.updateDictionary(dict)

Vue.use(VeeValidate, {
  locale: 'it'
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
